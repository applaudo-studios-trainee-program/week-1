# Trainee Program - Week 1

## Installation

There is **no necessary configuration** and you can just clone the repo

## Running the Project

You can use the link(s) below to see the last deployed version of the project or clone the repo, it's up to you

[GitLab Pages](https://applaudo-studios-trainee-program.gitlab.io/week-1/)  
[Vercel](https://week-1.b-mendoza.vercel.app/)

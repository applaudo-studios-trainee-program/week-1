{
  const formElement = document.querySelector('form');
  const firstNameElement = document.querySelector('#first-name');
  const lastNameElement = document.querySelector('#last-name');
  const emailElement = document.querySelector('#email');
  const passwordElement = document.querySelector('#password');
  const confirmPasswordElement = document.querySelector('#confirm-password');
  const ageElement = document.querySelector('#age');
  const phoneElement = document.querySelector('#phone');
  const websiteElement = document.querySelector('#website');
  const experienceElement = document.querySelector('#level-of-experience');
  const hoursElement = document.querySelector('#hours');

  // ---------------------------------------------------------------------------
  // Handles the display action of the easter egg if it's guessed
  // ---------------------------------------------------------------------------

  const displayEasterEgg = (code, gifURL) => {
    const easterEgg = document.createElement('div');

    const easterEggImage = document.createElement('img');
    const easterEggImageContainer = document.createElement('div');
    const easterEggTextSpan = document.createElement('span');
    const easterEggCode = document.createElement('span');
    const easterEggText = document.createElement('p');
    const easterEggButtonIcon = document.createElement('i');
    const easterEggButton = document.createElement('button');
    const easterEggContainer = document.createElement('div');
    const easterEggTitle = document.createElement('h2');

    easterEggTitle.classList.add('easter-egg__title');
    easterEggTitle.append('Nice, you have guessed the secret code');

    easterEggImage.classList.add('easter-egg__image');
    easterEggImage.setAttribute('src', gifURL);
    easterEggImage.setAttribute('alt', 'Cat GIF');

    easterEggImageContainer.classList.add('easter-egg__image-container');
    easterEggImageContainer.append(easterEggImage);

    easterEggTextSpan.append('Oh! I almost forgot. ');

    easterEggCode.classList.add('easter-egg__code');
    easterEggCode.append(code);

    easterEggText.classList.add('easter-egg__text');
    easterEggText.append(easterEggTextSpan);
    easterEggText.append('The secret code is... ');
    easterEggText.append(easterEggCode);
    easterEggText.append(", but... don't tell anybody!");

    easterEggButtonIcon.classList.add('fas', 'fa-minus');

    easterEggButton.classList.add('easter-egg__button');
    easterEggButton.append(easterEggButtonIcon);

    easterEggContainer.classList.add('easter-egg__container');
    easterEggContainer.append(easterEggTitle);
    easterEggContainer.append(easterEggImageContainer);
    easterEggContainer.append(easterEggText);

    easterEgg.classList.add('easter-egg');
    easterEgg.append(easterEggContainer);
    easterEgg.append(easterEggButton);

    easterEggButton.addEventListener('click', () => easterEgg.remove());

    if (document.body.contains(document.querySelector('.easter-egg'))) {
      document.querySelector('.easter-egg').remove();

      document.body.append(easterEgg);
    } else {
      document.body.append(easterEgg);
    }
  };

  // ---------------------------------------------------------------------------
  // Gets the URL of a random GIF of a cat
  // ---------------------------------------------------------------------------

  const getRandomCatGif = async () => {
    const response = await fetch(
      `https://api.giphy.com/v1/gifs/random?api_key=l2Dt97UyzPlTq7xjKzIQLaHeYtQFMcUb&tag=${encodeURI(
        'cats'
      )}&rating=g`
    );

    const { data } = await response.json();
    const { url } = data.images.downsized_medium;

    return url;
  };

  // ---------------------------------------------------------------------------
  // handles user typing to display the Easter Egg if it's guessed
  // ---------------------------------------------------------------------------

  const SECRET_CODE = '4ppl4ud0';
  const pressedKeys = [];

  const handleUserTyping = async ({ key }) => {
    pressedKeys.push(key);

    pressedKeys.splice(
      -SECRET_CODE.length - 1,
      pressedKeys.length - SECRET_CODE.length
    );

    const isCodeValid = pressedKeys.join('').toLowerCase() === SECRET_CODE;

    if (isCodeValid) {
      const gifURL = await getRandomCatGif();

      displayEasterEgg(SECRET_CODE, gifURL);
    }
  };

  window.addEventListener('keyup', handleUserTyping);

  // ---------------------------------------------------------------------------
  // Updates the UI when the input[type="range"] change it's value
  // ---------------------------------------------------------------------------

  const updateUIOnInputRangeChange = inputRange => {
    const rangeUIIndicator = document.querySelector('.form__range-indicator');

    rangeUIIndicator.innerHTML = `${inputRange.value} hrs`;
  };

  document.addEventListener('DOMContentLoaded', () => {
    updateUIOnInputRangeChange(hoursElement);
  });

  hoursElement.addEventListener('change', function () {
    updateUIOnInputRangeChange(this);
  });

  hoursElement.addEventListener('mousemove', function () {
    updateUIOnInputRangeChange(this);
  });

  // ---------------------------------------------------------------------------
  // Handles the inputs where it's mandatory to be alpha or numeric characters
  // ---------------------------------------------------------------------------

  const onlyLettersWhenTyping = event => {
    const isAlpha = /^[a-zA-Z]+$/g.test(event.key);

    if (!isAlpha) event.preventDefault();
  };

  const onlyNumbersWhenTyping = event => {
    const isNumeric = /^[0-9]+$/g.test(event.key);

    if (!isNumeric) event.preventDefault();
  };

  firstNameElement.addEventListener('keypress', onlyLettersWhenTyping);

  lastNameElement.addEventListener('keypress', onlyLettersWhenTyping);

  ageElement.addEventListener('keypress', onlyNumbersWhenTyping);

  phoneElement.addEventListener('keypress', onlyNumbersWhenTyping);

  phoneElement.addEventListener('input', function () {
    if (this.value.length > 0 && /^[0-9]+$/g.test(this.value)) {
      this.value = this.value.match(/[0-9]{1,4}/g).join('-');
    }
  });

  // ---------------------------------------------------------------------------
  // Handles the action to display the errors
  // ---------------------------------------------------------------------------

  const displayErrors = listOfErrors => {
    listOfErrors.map(({ inputId, message }) => {
      const errorContainer = document.createElement('span');
      const errorIcon = document.createElement('i');
      const errorMessage = document.createTextNode(message);

      errorIcon.classList.add('fas', 'fa-times', 'fa-lg');

      errorContainer.classList.add('error');
      errorContainer.append(errorIcon);
      errorContainer.append(errorMessage);

      const inputReference = document.querySelector(`#${inputId}`);

      inputReference.parentNode.insertBefore(
        errorContainer,
        inputReference.nextSibling
      );
    });
  };

  // ---------------------------------------------------------------------------
  // Handles the cleaning of the displayed errors
  // ---------------------------------------------------------------------------

  const cleanErrors = listOfDisplayedErrors => {
    listOfDisplayedErrors.map(error => error.remove());
  };

  // ---------------------------------------------------------------------------
  // Handles the input cleaning when a valid submit is done
  // ---------------------------------------------------------------------------

  const inputCleaningOnValidSubmit = () => {
    [
      ...document.querySelectorAll(
        '[type=text], [type=email], [type=password], [type=number], [type=tel], [type=url]'
      )
    ].map(input => {
      input.value = '';
    });

    experienceElement.selectedIndex = 0;

    [...document.querySelectorAll('[type=checkbox]')].map(input => {
      input.checked = false;
    });

    hoursElement.value = 40;
    hoursElement.dispatchEvent(new Event('change'));
  };

  // ---------------------------------------------------------------------------
  // Log the values when a valid submit is done
  // ---------------------------------------------------------------------------

  const logValues = ({
    firstName,
    lastName,
    age,
    email,
    password,
    phone,
    website,
    hearAboutUs,
    levelOfExperience,
    hoursAbleToWork
  }) => {
    console.clear();

    console.group(
      '%c%s%c %c%s',
      'font-size: 1.8em; color: #fff',
      'Welcome',
      'background: inherit',
      'font-size: 1.8em; color: #fff',
      `\nThis is your valid information`
    );

    if (!!firstName) {
      console.log(
        '%c%s%c %c%s',
        'font-size: 1.3em; color: #0f7ef1',
        'First Name:',
        'background: inherit',
        'font-size: 1.3em; color: #fff',
        `${firstName}`
      );
    }

    if (!!lastName) {
      console.log(
        '%c%s%c %c%s',
        'font-size: 1.3em; color: #0f7ef1',
        'Last Name:',
        'background: inherit',
        'font-size: 1.3em; color: #fff',
        `${lastName}`
      );
    }

    if (!!age) {
      console.log(
        '%c%s%c %c%s',
        'font-size: 1.3em; color: #0f7ef1',
        'Age:',
        'background: inherit',
        'font-size: 1.3em; color: #fff',
        `${age}`
      );
    }

    if (!!email) {
      console.log(
        '%c%s%c %c%s',
        'font-size: 1.3em; color: #0f7ef1',
        'Email:',
        'background: inherit',
        'font-size: 1.3em; color: #fff',
        `${email}`
      );
    }

    if (!!password) {
      console.log(
        '%c%s%c %c%s',
        'font-size: 1.3em; color: #0f7ef1',
        'Password:',
        'background: inherit',
        'font-size: 1.3em; color: #fff',
        `${password}`
      );
    }

    if (!!phone) {
      console.log(
        '%c%s%c %c%s',
        'font-size: 1.3em; color: #0f7ef1',
        'Phone:',
        'background: inherit',
        'font-size: 1.3em; color: #fff',
        `${phone}`
      );
    }

    if (!!website) {
      console.log(
        '%c%s%c %c%s',
        'font-size: 1.3em; color: #0f7ef1',
        'Website:',
        'background: inherit',
        'font-size: 1.3em; color: #fff',
        `${website}`
      );
    }

    if (hearAboutUs !== '') {
      console.log(
        '%c%s%c %c%s',
        'font-size: 1.3em; color: #0f7ef1',
        'Hear about us:',
        'background: inherit',
        'font-size: 1.3em; color: #fff',
        `${hearAboutUs}`
      );
    }

    if (!!levelOfExperience) {
      console.log(
        '%c%s%c %c%s',
        'font-size: 1.3em; color: #0f7ef1',
        'Level of Experience:',
        'background: inherit',
        'font-size: 1.3em; color: #fff',
        `${levelOfExperience}`
      );
    }

    console.log(
      '%c%s%c %c%s',
      'font-size: 1.3em; color: #0f7ef1',
      'Weekly hours able to work:',
      'background: inherit',
      'font-size: 1.3em; color: #fff',
      `${hoursAbleToWork} hours`
    );
  };

  // ---------------------------------------------------------------------------
  // Handles the submit action of the form
  // ---------------------------------------------------------------------------

  const handleSubmit = event => {
    event.preventDefault();

    // -------------------------------------------------------------------------
    // Handles the normalization of the values that come from the hear about us
    // list
    // -------------------------------------------------------------------------

    const normalizeHearAboutValueList = list => {
      return list
        .map(
          value =>
            value.charAt(0).toUpperCase() + value.substring(1, value.length)
        )
        .map(value => {
          if (/-/g.test(value)) {
            return value
              .replace(/-/g, ' ')
              .split(' ')
              .map(
                value =>
                  value.charAt(0).toUpperCase() +
                  value.substring(1, value.length)
              )
              .join(' ');
          } else return value;
        })
        .join(', ');
    };

    // -------------------------------------------------------------------------
    // Handles the normalization of the value that come from the experience
    // -------------------------------------------------------------------------

    const normalizeLevelOfExperience = string => {
      if (/-/.test(string)) {
        return string
          .replace(/-/g, ' ')
          .split(' ')
          .map(
            value =>
              value.charAt(0).toUpperCase() + value.substring(1, value.length)
          )
          .join(' ');
      } else {
        return (
          string.charAt(0).toUpperCase() + string.substring(1, string.length)
        );
      }
    };

    const firstNameValue = firstNameElement.value;
    const lastNameValue = lastNameElement.value;
    const emailValue = emailElement.value;
    const passwordValue = passwordElement.value;
    const confirmPasswordValue = confirmPasswordElement.value;
    const ageValue = ageElement.value;
    const phoneValue = phoneElement.value;
    const websiteValue = websiteElement.value;
    const experienceValue = experienceElement.value;
    const hoursValue = hoursElement.value;

    const hearAboutUsList = [
      ...document.querySelectorAll('[name=hear-about-us]:checked')
    ];

    const isValid = (regex, string) => regex.test(string);

    const data = {};
    const errors = [];

    if (firstNameValue.trim() !== '') {
      const isFirstNameValid = isValid(/^[a-zA-Z]+$/, firstNameValue.trim());

      isFirstNameValid
        ? (data.firstName = firstNameValue.trim())
        : errors.push({
            inputId: 'first-name',
            message: 'Only letters are valid'
          });
    } else {
      errors.push({
        inputId: 'first-name',
        message: "This input can't be empty"
      });
    }

    if (lastNameValue.trim() !== '') {
      const isLastNameValid = isValid(/^[a-zA-Z]+$/, lastNameValue.trim());

      isLastNameValid
        ? (data.lastName = lastNameValue.trim())
        : errors.push({
            inputId: 'last-name',
            message: 'Only letters are valid'
          });
    } else {
      errors.push({
        inputId: 'last-name',
        message: "This input can't be empty"
      });
    }

    if (emailValue.trim() !== '') {
      const isEmailValid = isValid(
        /^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/,
        emailValue.trim()
      );

      isEmailValid
        ? (data.email = emailValue.trim())
        : errors.push({
            inputId: 'email',
            message: 'Please type a valid email'
          });
    } else {
      errors.push({
        inputId: 'email',
        message: "This input can't be empty"
      });
    }

    if (passwordValue !== '') {
      if (passwordValue.length >= 6) {
        if (passwordValue === confirmPasswordValue) {
          data.password = passwordValue;
        } else {
          errors.push({
            inputId: 'confirm-password',
            message: 'Passwords are not the same'
          });
        }
      } else {
        errors.push({
          inputId: 'password',
          message: 'Passwords must be at least six characters long'
        });
      }
    } else {
      errors.push({
        inputId: 'password',
        message: "This input can't be empty"
      });
    }

    if (ageValue.trim() !== '') {
      const isAgeValid = isValid(/^[0-9]{1,2}$/, ageValue.trim());

      if (isAgeValid) {
        parseFloat(ageValue) >= 18
          ? (data.age = ageValue.trim())
          : errors.push({
              inputId: 'age',
              message: 'You must be 18 years old at least'
            });
      } else {
        errors.push({
          inputId: 'age',
          message: 'Please type a valid age'
        });
      }
    } else {
      errors.push({
        inputId: 'age',
        message: "This input can't be empty"
      });
    }

    if (phoneValue.trim() !== '') {
      const isPhoneValid = isValid(/^[0-9]{4}-[0-9]{4}$/, phoneValue.trim());

      isPhoneValid
        ? (data.phone = phoneValue.trim())
        : errors.push({
            inputId: 'phone',
            message: 'Please type a valid phone number'
          });
    } else {
      errors.push({
        inputId: 'phone',
        message: "This input can't be empty"
      });
    }

    if (websiteValue.trim() !== '') {
      const isWebsiteValid = isValid(
        /https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,4}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/,
        websiteValue.trim()
      );

      isWebsiteValid
        ? (data.website = websiteValue.trim())
        : errors.push({
            inputId: 'website',
            message: 'Please type a valid URL'
          });
    }

    const hearAboutUsValueList = hearAboutUsList.map(element => element.value);

    data.hearAboutUs = normalizeHearAboutValueList(hearAboutUsValueList);

    experienceValue !== 'Select an option'
      ? (data.levelOfExperience = normalizeLevelOfExperience(experienceValue))
      : errors.push({
          inputId: 'level-of-experience',
          message: 'Please select an option'
        });

    data.hoursAbleToWork = hoursValue;

    const listOfErrors = [...document.querySelectorAll('.error')];

    cleanErrors(listOfErrors);
    logValues(data);

    errors.length > 0 ? displayErrors(errors) : inputCleaningOnValidSubmit();
  };

  formElement.addEventListener('submit', handleSubmit);
}
